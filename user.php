<?php
include_once(dirname(__FILE__) . '/model.php'); 
class User extends Model
{
    public $id;
    public $name;
    public $email;

    public function create() {
        return "INSERT INTO user (id, name, email) VALUES (:$this->id, :$this->name, :$this->email)";
    }

    public function update() {
        return "UPDATE user SET name = :$this->name, email = '$this->email' WHERE id = :$this->id ";
    }

    public function save() {
        if(isset($this->id)) {  //По наличию id определяем, есть ли такая запись в базе данных
            $result = $this->update();
            return $result;
        } else {
            $result = $this->create();
            return $result;
        }
    }
    public function delete() {
        return "DELETE user WHERE id = :$this->id";
    }
}
?>