<?php
abstract class Model
{
    public static function find($id, $table) {
        return 'SELECT * FROM ' . $table . ' WHERE id = :' . $id;
    }

    abstract protected function create();

    abstract protected function update();

    abstract protected function delete();
}
?>