<?php
include_once(dirname(__FILE__) . '/user.php'); 

$user = new User;

$user->name = 'John';
$user->email = 'some@gmail.com';
$result = $user->save();
var_dump($result);
echo '<br/>';
$user->id = 1;
$result = $user->save();
var_dump($result);
echo '<br/>';
$userFind = User::find(1, 'user');
var_dump($userFind);
echo '<br/>';
$result2 = $user->delete();
var_dump($result2);
echo '<br/>';

$secondUser = new User;

$secondUser->name = 'Jack';
$secondUser->email = 'somejack@gmail.com';
$resultSecondUser = $secondUser->save();
var_dump($resultSecondUser);
echo '<br/>';
$secondUser->id = 2;
$secondUser->name = 'Tom';
$secondUser->email = 'sometommy@gmail.com';
$onemoreResult = $secondUser->save();
var_dump($onemoreResult);
echo '<br/>';
?>